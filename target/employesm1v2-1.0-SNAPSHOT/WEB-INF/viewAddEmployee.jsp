<%-- 
    Document   : viewAddEmployee
    Created on : 29 nov. 2018, 08:26:18
    Author     : Quentin Lathaud
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ajout d'un employé</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />    
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" /> 
    </head>
    <body>
        <div class="container">
            <div class="row justify-content-end">
                <p>Votre session est active</p>&nbsp;
                <a href="login" alt="logout"> <i class="fa fa-sign-out"></i> </a>
            </div>
        <h2 class="h2">Ajout d'un employé</h1>
            
        <form method="POST" action="recordEmployee" class="form-horizontal">

                <div class="form-group row">
                    <div class="col-sm-12">
                        <label>Nom </label>
                        <input type="text" class="form-control" aria-label="Nom" aria-describedby="basic-addon1" name="nom" >
                    </div>
                </div>
                
                <div class="form-group row">
                     <div class="col-sm-12">
                         <label> Prénom </label>
                        <input type="text" class="form-control" aria-label="Nom" aria-describedby="basic-addon1" name="prenom"/>
                     </div>
                </div>

                <div class="form-group row">
                     <div class="col-sm-12">
                        <label> Tél dom </label>
                        <input type="text" class="form-control" aria-label="Nom" aria-describedby="basic-addon1" name="teldom"/>
                     </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label> Tél mob </label>
                        <input type="text" class="form-control" aria-label="Nom" aria-describedby="basic-addon1" name="telmob"/>
                    </div>
                    <div class="col-sm-6">
                        <label> Tél pro </label>
                        <input type="text" class="form-control" aria-label="Nom" aria-describedby="basic-addon1" name="telpro"/>
                   </div>
                </div>
                
              
                
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label> Adresse </label>
                         <input type="text" class="form-control" aria-label="Nom" aria-describedby="basic-addon1" name="adresse" name="adresse"/>
                    </div>
                    
                    <div class="col-sm-6">
                        <label> Code postal </label>
                        <input type="text" class="form-control" aria-label="Nom" aria-describedby="basic-addon1" name="codepostal"/>
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label> Ville </label>
                        <input type="text" class="form-control" aria-label="Nom" aria-describedby="basic-addon1" name="ville"/>
                    </div>
                    
                    <div class="col-sm-6">
                        <label>Adresse e-mail </label>
                        <input type="text" class="form-control" aria-label="Nom" aria-describedby="basic-addon1" name="email"/>
                    </div>
                </div>
               
            </table>
            
            
                <input type="submit" value="Valider" class="btn btn-primary" />&nbsp;
                <a href="../employesm1v2" class="btn btn-info">Voir liste</a><br><br>
               
            
            <div class="row justify-content-start">
                 <strong style="color:red;">${error}</strong>
            </div>
        </form>
        <br/>       
        </div>
            
        
    </body>
</html>
