
<%@taglib uri="http://java.sun.com/jsp/jstl/core"prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bonjour les employés !</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />  
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    </head>
    <body>  
        <div class="container">
            <div class="row justify-content-end">
                <p>Votre session est active</p>&nbsp;
                <form action='/employesm1v2/disconnect' method="POST">
                    <button class="btn btn-info"><i class="fa fa-sign-out"></i> </button>
                </form>
            </div>
        <h1 class="h1">Liste des employés</h1>
        <hr><br>
        <form id="mainForm" action="" method="GET">
            <table class="table table-responsive table-bordered table-hover">
           <thead class="thead-dark">
             <tr>
               <th scope="col">#</th>
               <th scope="col">Prenom</th>
               <th scope="col">Nom</th>
               <th scope="col">Email</th>
               <th scope="col">Tel Dom</th>
               <th scope="col">Tel Pro</th>
               <th scope="col">Tel Port</th>
               <th scope="col">Adresse</th>
               <th scope="col">Code Postal</th>
               <th scope="col">Ville</th>
             </tr>
           </thead>
           <tbody>
               <c:forEach items="${employes}" var="employe" varStatus="status">  
               <tr>
               <th scope="row">${status.count}</th>
               <td>${employe[0]}</td>
               <td>${employe[1]}</td>
               <td>${employe[2]}</td>
               <td>${employe[3]}</td>
               <td>${employe[4]}</td>
               <td>${employe[5]}</td>
               <td>${employe[6]}</td>
               <td>${employe[7]}</td>
               <td>${employe[8]}</td>
              </tr>
              </c:forEach>
             <tr>
           </tbody>
         </table>
     <div style="display:flex">

                    <div class="emp_actions" style="margin-right:5px">

                        
                        <button formaction="/employesm1v2/deleteEmployee" class="btn btn-primary submit-link">Supprimer</button>
                        <button formaction="/employesm1v2/modifyEmployee" class="btn btn-primary submit-link">Détails</button>
                    </div>
                    <br><br>
            </form>
                    <form action="" method="GET">
                        <div>
                                <button formaction="/employesm1v2/addEmployee" class="btn btn-success">Ajouter </button>
                        </div>
                    </form>
                </div> 
</div>
</body>
</html>
