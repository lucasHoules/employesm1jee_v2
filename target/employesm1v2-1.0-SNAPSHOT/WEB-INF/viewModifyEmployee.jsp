<%-- 
    Document   : viewModifyEmployee
    Created on : 29 nov. 2018, 08:58:21
    Author     : pc-benoit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modification utilisateur</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" /> 
    </head>
    <body>
       <div class="container">
           <div class="row justify-content-end">
                <p>Votre session est active</p>&nbsp;
                <a href="login" alt="logout"> <i class="fa fa-sign-out"></i> </a>
            </div>
           <h1>Détail du membre ${employeDetails.prenom} ${employeDetails.nom} </h1><br>
        <form action="updatingEmployee" method="POST">
             <input class="form-control" id="id" type="hidden" name="id" value="${employeDetails.id}"/>
            <div class="form-group">
                <label for="nom">Nom</label>
                <input class="form-control" id="nom" type="text" name="nom" value="${employeDetails.nom}"/>
            </div>
            
            <div class="form-group">
                <label for="prenom">Prénom</label>
                <input class="form-control" id="prenom" type="text" name="prenom" value="${employeDetails.prenom}"/>
            </div>

             <div class="form-group">
                <label for="teldom">Tél dom</label>
                <input class="form-control" id="teldom" type="text" name="teldom" value="${employeDetails.teldom}"/>
             </div>
            
            <div class="form-group">
                <label for="telmob">Tél mob</label>
                <input class="form-control" id="telmob" type="text" name="telmob" value="${employeDetails.telport}"/>
            </div>
            
             <div class="form-group">
                <label for="telpro">Tél pro</label>
                <input class="form-control" id="telpro" type="text" name="telpro" value="${employeDetails.telpro}"/>
             </div>
            
             <div class="form-group">
                <label for="adresse">Adresse</label>
                <input class="form-control" id="adresse" type="text" name="adresse" value="${employeDetails.adresse}"/>
             </div>
            
             <div class="form-group">
                <label for="codepostal">Code postal</label>
                <input class="form-control" id="codepostal" type="number" name="codepostal" value="${employeDetails.codepostal}"/>
             </div>
            
             <div class="form-group">
                <label for="ville">Ville</label>
                <input class="form-control" id="ville" type="text" name="ville" value="${employeDetails.ville}"/>
             </div>
            
             <div class="form-group">
                <label for="email">Adresse e-mail</label>
                <input class="form-control" id="email" type="email" name="email" value="${employeDetails.email}"/>
             </div>
            
            <input type="submit" class="btn btn-primary" value="Modifier"></button>

            <a href="../employesm1v2" class="btn btn-info">Voir liste</a><br><br>
            </form>
     </div>
    </body>
</html>
