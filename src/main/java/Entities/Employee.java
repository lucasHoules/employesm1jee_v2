package Entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;

@Entity
@Table(name = "EMPLOYES")
public class Employee implements Serializable {

    @Id
    @Column( name = "ID")
    private int id;
    @Column( name = "NOM")
    private String nom;
    @Column( name = "PRENOM")
    private String prenom;
    @Column( name = "TELDOM")
    private String teldom;
    @Column( name = "TELPORT")
    private String telport;
    @Column( name = "TELPRO")
    private String telpro;
    @Column( name = "ADRESSE")
    private String adresse;
    @Column( name = "CODEPOSTAL")
    private String codepostal;
    @Column( name = "VILLE")
    private String ville;
    @Column( name = "EMAIL")
    private String email;
    
    public Employee() {
        
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the teldom
     */
    public String getTeldom() {
        return teldom;
    }

    /**
     * @param teldom the teldom to set
     */
    public void setTeldom(String teldom) {
        this.teldom = teldom;
    }

    /**
     * @return the telport
     */
    public String getTelport() {
        return telport;
    }

    /**
     * @param telport the telport to set
     */
    public void setTelport(String telport) {
        this.telport = telport;
    }

    /**
     * @return the telpro
     */
    public String getTelpro() {
        return telpro;
    }

    /**
     * @param telpro the telpro to set
     */
    public void setTelpro(String telpro) {
        this.telpro = telpro;
    }

    /**
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * @return the codepostal
     */
    public String getCodepostal() {
        return codepostal;
    }

    /**
     * @param codepostal the codepostal to set
     */
    public void setCodepostal(String codepostal) {
        this.codepostal = codepostal;
    }

    /**
     * @return the ville
     */
    public String getVille() {
        return ville;
    }

    /**
     * @param ville the ville to set
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
     public static boolean isEmployeComplet (HttpServletRequest request){
        return !(request.getParameter("nom").isEmpty() || request.getParameter("prenom").isEmpty() || request.getParameter("teldom").isEmpty() ||request.getParameter("telmob").isEmpty() || request.getParameter("telpro").isEmpty() ||request.getParameter("email").isEmpty() || request.getParameter("codepostal").isEmpty() || request.getParameter("adresse").isEmpty() || request.getParameter("ville").isEmpty());
    }
   
}
