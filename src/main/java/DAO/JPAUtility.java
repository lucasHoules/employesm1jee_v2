package DAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtility {
    
    private static final EntityManagerFactory emFactory;
    
    static {
        emFactory = Persistence.createEntityManagerFactory("projet");
               
    }
    
    /**
     * get the entity manager
     * @return the entity manager
     */
    public static EntityManager getEntityManager(){
        return emFactory.createEntityManager();
    }
    
    /**
     * close the entityManager connection
     */
    public static void close(){
        emFactory.close();
    }
} 
