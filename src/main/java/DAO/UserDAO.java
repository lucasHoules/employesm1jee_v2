package DAO;

import Entities.User;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class UserDAO extends AbstractDAO<User>{
    
    private EntityManager em = JPAUtility.getEntityManager();
    
    /**
     * Vérifie si l'utilisateur existe
     * @param username
     * @param password
     * @return l'utilisateur
     */
    public List<User> findByUsernamePassword(String username,String password) {
        Query query = em.createQuery("SELECT u from User u WHERE u.username=:username AND u.password=:password");
        query.setParameter("username", username);
        query.setParameter("password", password);
        return query.getResultList();
    }
    
    /**
     * Trouve un utilisateur par id
     * @param id Id de l'utilisateur à trouver
     * @return renvoie l'utilisateur trouvé
     */
    @Override
    public User find(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Créer l'utilisateur
     * @param obj utilisateur créer
     * @return 
     */
    @Override
    public boolean create(User obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Met à jour l'utilisateur
     * @param obj utilisateur à mettre à jour
     * @return Utilisateur mis à jour
     */
    @Override
    public User update(User obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Supprime l'utilisateur
     * @param obj utilisateur à supprimer
     * @return Suppression de l'objet selon un booléen
     */
    @Override
    public boolean delete(User obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
