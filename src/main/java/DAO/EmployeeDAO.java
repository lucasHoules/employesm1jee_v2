package DAO;

import javax.persistence.EntityManager;
import Entities.Employee;
import java.util.List;

/**
 * Classe héritant du DAO avec le type Employee
 * @author Quentin Lathaud
 */
public class EmployeeDAO extends AbstractDAO<Employee> {
            
    private EntityManager em= JPAUtility.getEntityManager();
    
    /**
     * Réalise une recherche par id en bdd et retourne l'employe
     * @param id Id de l'employé à trouver
     * @return L'employé trouvé dans la recherche
     */
    @Override
    public Employee find(int id) {        
        Employee employe = em.find(Employee.class,id);
       // em.close();
        return employe;
    }

    /**
     * Créé un employé dans la bdd
     * @param employee Prend l'objet Employee à ajouter 
     * @return L'employé venant d'être inséré en bdd
     * @throws java.sql.SQLException
     */
    @Override
    public boolean create(Employee employee) {
        int nbEmployesAvant=findAll().size();
        em.getTransaction().begin();
        em.persist(employee);
        em.getTransaction().commit();
	if(findAll().size()>nbEmployesAvant){
            return true;
        }
        return false;
    }

    /**
     * Met à jour un employé
     * @param employee Employé déjà modifié, prêt à être modifié en bdd
     * @return Renvoie l'employé modifié
     */
    @Override
    public Employee update(Employee employee) {
        em.getTransaction().begin();	
        em.merge(employee);
        em.getTransaction().commit();
        return employee;
    }

    /**
     * Supprime l'employé via son Id en bdd
     * @param employee Employé à supprimer
     * @return Renvoie un booléen en fonction de la réussite ou l'échec de la suppression
     */
    @Override
    public boolean delete(Employee employee) {
        
        employee = find(employee.getId());
        em.getTransaction().begin();
        em.remove(employee);
        em.getTransaction().commit();
        if(find(employee.getId()) != null){
            return false;
        }
        return true;
    }
    
    /**
     * Remplit un objet employé depuis le résultat d'une requête SQL
     * @return Employé remplit via la requête
     */
    public List<Employee> findAll(){
        List<Employee> employes=em.createQuery("SELECT e FROM Employee e").getResultList();
        return employes;
    }
    
}
