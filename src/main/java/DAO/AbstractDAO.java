package DAO;

public abstract class AbstractDAO<T> {
    
    /**
     *
     */
   
	
	/**
	 * Permet de récupérer un objet via son ID
	 * @param id
	 * @return
	 */
	public abstract T find(int id);
	
	/**
	 * Permet de créer une entrée dans la base de données
	 * par rapport à un objet
	 * @param obj
         * @return 
	 */
	public abstract boolean  create(T obj);
	
	/**
	 * Permet de mettre à jour les données d'une entrée dans la base 
	 * @param obj
         * @return 
	 */
	public abstract T update(T obj);
	
	/**
	 * Permet la suppression d'une entrée de la base
         * @param id
         * @return 
	 */
	public abstract boolean delete(T obj);
}
